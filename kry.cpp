/*
 * @brief  KRY 2017/2018 Implementation and breaking of RSA
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   05/2018
 */

/* System and standard library headers. */
#include <cerrno>
#include <cstdlib>
#include <ctime>
#include <exception>
#include <iostream>
#include <string>

#include <gmpxx.h>

/* Minimal number of bits for generated modulus (source: forum post by the project assignor). */
#define MIN_MODULUS_BITS 6

/* Accuracy for primality test. Number of iterations with Miller-Rabin test. */
#define PRIMALITY_ACCURACY 10

/* Upper limit for trivial division factorization. */
#define TRIVIAL_FACTOR_MAX 1000000

#define UNUSED(x) ((void)(x))

enum commands {
    CMD_UNKNOWN,
    CMD_GENERATE,
    CMD_ENCIPHER,
    CMD_DECIPHER,
    CMD_BREAKRSA,
};

struct command {
    enum commands type;
    std::string B;
    std::string N;
    std::string E;
    std::string D;
    std::string M;
    std::string C;
};

/**
 * @brief Prints program usage info.
 */
static void
print_usage(std::ostream &out, std::string name)
{
    out << "Usage:" << std::endl;
    out << "    " << name << " -g B | -e E N M | -d D N C | -b N" << std::endl;
    out << "        -g ... generate key" << std::endl;
    out << "        -e ... encipher" << std::endl;
    out << "        -d ... decipher" << std::endl;
    out << "        -b ... break RSA" << std::endl;
    out << "        B ... size of public modulus in bits" << std::endl;
    out << "        N ... public modulus (hex number 0x...)" << std::endl;
    out << "        E ... public exponent (hex number 0x...)" << std::endl;
    out << "        D ... private exponent (hex number 0x...)" << std::endl;
    out << "        M ... plain message (hex number 0x...)" << std::endl;
    out << "        C ... cipher message (hex number 0x...)" << std::endl;
}

/**
 * @brief Parses and validates command line arguments.
 * @return 0 on success, error code on error.
 */
static int
parse_args(int argc, char *argv[], struct command &cmd, std::ostream &err)
{
    std::string command_arg;
    std::string name = std::string(argv[0]);

    cmd.type = CMD_UNKNOWN;

    if (argc < 2) {
        err << "Missing arguments!" << std::endl;
        print_usage(err, name);
        return EINVAL;
    }

    command_arg = std::string(argv[1]);

    if ((command_arg.compare("-g") == 0) && (argc == 3)) {
        cmd.type = CMD_GENERATE;
        cmd.B = std::string(argv[2]);
    } else if ((command_arg.compare("-e") == 0) && (argc == 5)) {
        cmd.type = CMD_ENCIPHER;
        cmd.E = std::string(argv[2]);
        cmd.N = std::string(argv[3]);
        cmd.M = std::string(argv[4]);
    } else if ((command_arg.compare("-d") == 0) && (argc == 5)) {
        cmd.type = CMD_DECIPHER;
        cmd.D = std::string(argv[2]);
        cmd.N = std::string(argv[3]);
        cmd.C = std::string(argv[4]);
    } else if ((command_arg.compare("-b") == 0) && (argc == 3)) {
        cmd.type = CMD_BREAKRSA;
        cmd.N = std::string(argv[2]);
    }

    if (cmd.type == CMD_UNKNOWN) {
        err << "Wrong count or format of arguments!" << std::endl;
        return EINVAL;
    } else {
        return 0;
    }
}

/**
 * @brief Generates random integer number in range <lower,upper>.
 * @param rg Random number generator.
 * @param lower Lower bound of range.
 * @param upper Upper bound of range.
 * @return Random number.
 */
static mpz_class
gen_rand(gmp_randclass &rg, mpz_class lower, mpz_class upper)
{
    mpz_class num = lower + rg.get_z_range(upper - lower + 1);
    return num;
}

/**
 * @brief Generates odd random number with n-th bit set to 1 (counting from 1).
 * @param rg Random number generator.
 * @param n Count of bits for generated number.
 * @param set_bit If true (-1)-th bit is set to 1.
 * @return Random number.
 */
static mpz_class
gen_rand_odd(gmp_randclass &rg, const unsigned long n, bool set_bit)
{
    mpz_class num = rg.get_z_bits(n);
    mpz_setbit(num.get_mpz_t(), n - 1);
    if (set_bit)
        mpz_setbit(num.get_mpz_t(), n - 2);
    mpz_setbit(num.get_mpz_t(), 0);
    return num;
}

/**
 * @brief Performs Miller-Rabin test for primality.
 * @param num Number tested for primality.
 * @param d Odd number for which it is valid:
 *          d * (2^r) = num - 1, r >= 1
 * @param rg Random number generator.
 * @return false if num is not prime, true if probably is.
 */
static bool
miller_rabin_test(mpz_class num, mpz_class d, gmp_randclass &rg)
{
    mpz_class a;
    mpz_class x;

    a = gen_rand(rg, 2, num - 2);

    mpz_powm_sec(x.get_mpz_t(), a.get_mpz_t(), d.get_mpz_t(), num.get_mpz_t());

    if ((x == 1) || (x == (num - 1)))
        return true;

    while (d != num - 1) {
        x = (x * x) % num;
        d = d * 2;
        if (x == 1)
            return false;
        if (x == (num - 1))
            return true;
    }

    return false;
}

/**
 * @brief Checks if number is probably a prime or not.
 * @param num Number tested for primality.
 * @param accuracy Sets the number of iterations for Miller-Rabin test.
 *                 Higher the number, more precise the result.
 * @param rg Random number generator.
 * @return true if num is probably prime, false if not.
 */
static bool
is_prime(mpz_class num, int accuracy, gmp_randclass &rg)
{
    mpz_class d;
    int i;

    if ((num <= 1) || (num % 2 == 0))
        return false;
    if (num <= 3)
        return true;

    d = num - 1;
    while (d % 2 == 0)
        d = d / 2;

    for (i = 0; i < accuracy; i++)
        if (miller_rabin_test(num, d, rg) == false)
            return false;

    return true;
}

/*
 * @brief Generates random prime number.
 * @param rg Random number generator.
 * @param n Count of bits for generated number.
 * @param set_bit If true (-1)-th bit is set to 1.
 * @return Random prime number.
 */
static mpz_class
gen_prime(gmp_randclass &rg, const unsigned long bits, bool set_bit)
{
    mpz_class max(0);
    mpz_class num = gen_rand_odd(rg, bits, set_bit);
    mpz_setbit(max.get_mpz_t(), bits);
    while (is_prime(num, PRIMALITY_ACCURACY, rg) == false) {
        num += 2;
        if (num >= max)
            num = gen_rand_odd(rg, bits, set_bit);
    }
    return num;
}

/**
 * @return Greatest Common Divisor of two numbers 'a' and 'b'.
 */
static mpz_class
gcd(mpz_class a, mpz_class b)
{
    mpz_class t;
    while (b != 0) {
        t = b;
        b = a % b;
        a = t;
    }
    return a;
}

static int
inverse(mpz_class &result, mpz_class a, mpz_class n)
{
    mpz_class t_tmp;
    mpz_class r_tmp;
    mpz_class t(0);
    mpz_class newt(1);
    mpz_class r(n);
    mpz_class newr(a);
    while (newr != 0) {
        mpz_class quot = r / newr;
        t_tmp = t;
        t = newt;
        newt = t_tmp - quot * newt;
        r_tmp = r;
        r = newr;
        newr = r_tmp - quot * newr;
    }
    if (r > 1)
        return -1;
    if (t < 0)
        t = t + n;
    result = t;
    return 0;
}

/**
 * @brief Generates key.
 * @param out Output stream.
 * @param err Error stream.
 * @param B Requested size of public modulus in bits.
 * @return 0 on success, error code on error
 */
static int
generate_key(std::ostream &out, std::ostream &err, std::string &B)
{
    int ret;
    mpz_class seed;
    mpz_class mpz_B;
    unsigned long modulus_size;
    gmp_randclass rg(gmp_randinit_default);
    mpz_class P(0);
    mpz_class Q(0);
    mpz_class N;
    mpz_class E;
    mpz_class D;
    mpz_class phi;

    try {
        seed = time(NULL);
        mpz_B = B;
        rg.seed(seed);
        if (mpz_B < MIN_MODULUS_BITS) {
            err << "Too few bits (" << mpz_B <<
                ") for generating random numbers!" << std::endl;
            return EINVAL;
        }
        modulus_size = mpz_B.get_ui();
    } catch (std::invalid_argument &e) {
        err << e.what() << std::endl;
        return EINVAL;
    }

    do {
        bool set_bit = !(modulus_size <= 8 && modulus_size % 2 == 0);
        P = gen_prime(rg, modulus_size / 2, set_bit);
        do {
            Q = gen_prime(rg, modulus_size - (modulus_size / 2), set_bit);
        } while (P == Q);
        N = P * Q;
        phi = (P - 1) * (Q - 1);
        E = gen_rand(rg, 3, phi - 1);
        while (gcd(E, phi) != 1) {
            E++;
        }
        ret = inverse(D, E, phi);
        if (ret != 0)
            err << "E(" << E << ") is not invertible!" << std::endl;
    } while (ret != 0);

    out << "0x" << P.get_str(16);
    out << " ";
    out << "0x" << Q.get_str(16);
    out << " ";
    out << "0x" << N.get_str(16);
    out << " ";
    out << "0x" << E.get_str(16);
    out << " ";
    out << "0x" << D.get_str(16);
    out << std::endl;

    return 0;
}

/**
 * @brief Enciphers plain message.
 * @param out Output stream.
 * @param err Error stream.
 * @param E Public exponent.
 * @param N Public modulus.
 * @param M Plain message.
 * @return 0 on success, error code on error.
 */
static int
encipher(std::ostream &out, std::ostream &err, std::string &E,
        std::string &N, std::string &M)
{
    mpz_class C;
    mpz_class mpz_E;
    mpz_class mpz_N;
    mpz_class mpz_M;

    try {
        mpz_E = E;
        mpz_N = N;
        mpz_M = M;
    } catch (std::invalid_argument &e) {
        err << e.what() << std::endl;
        return EINVAL;
    }
    mpz_powm_sec(C.get_mpz_t(), mpz_M.get_mpz_t(), mpz_E.get_mpz_t(), mpz_N.get_mpz_t());
    out << "0x" << C.get_str(16) << std::endl;
    return 0;
}

/**
 * @brief Deciphers cipher message.
 * @param out Output stream.
 * @param err Error stream.
 * @param D Private exponent.
 * @param N Public modulus.
 * @param C Cipher message.
 * @return 0 on success, error code on error.
 */
static int
decipher(std::ostream &out, std::ostream &err, std::string &D,
        std::string &N, std::string &C)
{
    mpz_class M;
    mpz_class mpz_D;
    mpz_class mpz_N;
    mpz_class mpz_C;

    try {
        mpz_D = D;
        mpz_N = N;
        mpz_C = C;
    } catch (std::invalid_argument &e) {
        err << e.what() << std::endl;
        return EINVAL;
    }
    mpz_powm_sec(M.get_mpz_t(), mpz_C.get_mpz_t(), mpz_D.get_mpz_t(), mpz_N.get_mpz_t());
    out << "0x" << M.get_str(16) << std::endl;
    return 0;
}

/**
 * @brief g(x) function from Pollard's rho algorithm.
 * @return Result of g(x) function.
 *
 * g(x) = (x^2 + 1) mod n
 */
static mpz_class
g_func(mpz_class x, mpz_class n)
{
    return (x * x + 1) % n;
}

/**
 * @brief Implementation of Pollard's rho algorithm.
 * @param res Resulting factor.
 * @param n Factorized number.
 * @param g g(x) function.
 * @param start Initialization value.
 * @return true if factor found, false if not.
 */
static bool
pollard_rho(mpz_class &res, mpz_class n, mpz_class (*g)(mpz_class, mpz_class),
        mpz_class start)
{
    mpz_class x(start);
    mpz_class y(start);
    mpz_class d(1);

    while (d == 1) {
        x = g(x, n);
        y = g(g(y, n), n);
        d = gcd(abs(x - y), n);
    }

    if (d == n)
        return false;

    res = d;
    return true;
}

/**
 * @brief Breaks RSA.
 * @param out Output stream.
 * @param err Error stream.
 * @param N Public modulus.
 * @return 0 on success, error code on error.
 */
static int
break_rsa(std::ostream &out, std::ostream &err, std::string &N)
{
    mpz_class i;
    mpz_class mpz_N;
    mpz_class P(0);

    try {
        mpz_N = N;
    } catch (std::invalid_argument &e) {
        err << e.what() << std::endl;
        return EINVAL;
    }

    /* Try trivial factorization. */
    if (mpz_N % 2 != 0) {
        for (i = 3; i <= TRIVIAL_FACTOR_MAX; i += 2) {
            if (mpz_N % i == 0) {
                P = i;
                break;
            }
        }
    } else {
        P = 2;
    }

    if (P == 0) {
        /* Trivial factorization unsuccessfull, try Pollard's rho algorithm. */
        i = 2;
        while (pollard_rho(P, mpz_N, g_func, i) == false)
            i++;
    }

    out << "0x" << P.get_str(16) << std::endl;
    return 0;
}

/**
 * @brief Executes command.
 * @return 0 on success, error code on error.
 */
static int
execute_cmd(struct command &cmd, std::ostream &out, std::ostream &err)
{
    switch (cmd.type) {
    case CMD_GENERATE:
        return generate_key(out, err, cmd.B);
    case CMD_ENCIPHER:
        return encipher(out, err, cmd.E, cmd.N, cmd.M);
    case CMD_DECIPHER:
        return decipher(out, err, cmd.D, cmd.N, cmd.C);
    case CMD_BREAKRSA:
        return break_rsa(out, err, cmd.N);
    default:
        err << "Invalid operation - " << cmd.type << std::endl;
        return EINVAL;
    }
}

int
main(int argc, char *argv[])
{
    int ret;
    struct command cmd;

    ret = parse_args(argc, argv, cmd, std::cerr);
    if (ret != 0)
        return ret;

    ret = execute_cmd(cmd, std::cout, std::cerr);
    if (ret != 0)
        return ret;

    return EXIT_SUCCESS;
}
