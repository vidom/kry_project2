#
# KRY 2017/2018 Implementation and breaking of RSA
# Author: Matej Vido, xvidom00@stud.fit.vutbr.cz
# Date:   05/2018

NAME=kry
LOGIN=xvidom00
ZIPFILE=$(LOGIN).zip
DOC=doc.pdf

CXX=g++
CXXFLAGS=-std=c++11
DFLAGS=
#DFLAGS=-Wall -Wextra -pedantic -g

all: $(NAME)

$(NAME): kry.cpp
	$(CXX) $(CXXFLAGS) $(DFLAGS) $< -o $@ -lgmpxx -lgmp

.PHONY: clean purge pack doc

clean:
	rm -f *.o

purge:
	rm -f *.o $(NAME)

pack:
	rm -f $(ZIPFILE)
	zip $(ZIPFILE) kry.cpp Makefile $(DOC)

doc:
	make -C doc
	cp doc/doc.pdf $(DOC)
